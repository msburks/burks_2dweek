﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class PlayerMovement : MonoBehaviour
{
    //sets all necessary variables for the script
    Rigidbody2D rb2D;

    public float runSpeed;
    public float jumpForce;
    public float fadeDuration = 1f;
    public float displayImageDuration = 1f;
    public GameObject winTextObject;
    public SpriteRenderer spriteRenderer;
    public Animator animator;
    public TextMeshProUGUI counter_text;
    //public CanvasGroup winImage;
    public float winAmount;
    float counter;
    public float m_Timer;
    public float lives;
    public TextMeshProUGUI lives_text;
    public Scene load_scene;
    bool win;
    bool death;
    public AudioSource jumpSound;
    public AudioSource pickUpSound;



    // Start is called before the first frame update
    void Start()
    {
        rb2D = GetComponent<Rigidbody2D>();

        winTextObject.SetActive(false);
    }

    // Update is called once per frame
    private void Update()
    {
        //checking to see if space bar is pressed, and if on level layer
        if (Input.GetButtonDown("Jump"))
        {
            int levelMask = LayerMask.GetMask("Level");
            

            if (Physics2D.BoxCast(transform.position, new Vector2(1f, .1f),0f, Vector2.down, .01f, levelMask))
            {
                Jump();
            }
            
        }
        //trying to use the same type of indication for allowing the player to jump, for killing enemies
        int enemyMask = LayerMask.GetMask("Enemy");
        if (Physics2D.BoxCast(transform.position, new Vector2(1f, .1f), 0f, Vector2.down, .01f, enemyMask))
        {
            Kill();
        }
        //set botht text UI elements into the scene
        SetCountText();
        SetLivesText();

        //if (transform.position.y == -5f)// threshold for calling scenemanager stuff
        //{
        //    SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        //}

        //activates level restart
        if (death == true)
        {
            EndLevel();
        }
        //if (win == true)
            //EndLevelWin(winImage);
    }

    void FixedUpdate()
    {
        //allows the player to move
        float horizontalInput = Input.GetAxis("Horizontal");

        rb2D.velocity = new Vector2(horizontalInput * runSpeed * Time.deltaTime, rb2D.velocity.y);
        //flips the sprite
        if (rb2D.velocity.x > 0)
            spriteRenderer.flipX = false;
        else
        if (rb2D.velocity.x < 0)
            spriteRenderer.flipX = true;
        //sets conditions for animation transition
        if (Mathf.Abs(horizontalInput) > 0f)
            animator.SetBool("isRunning", true);
        else
            animator.SetBool("isRunning", false);


    }

    //allows player to jump, activates sound
    void Jump()
    {
        rb2D.velocity = new Vector2(rb2D.velocity.x, jumpForce);
        jumpSound.Play();
    }

    //function to kill the enemy
    void Kill()
    {
        
    }

    //call for a level restart
    void EndLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    //sets up all trigger collisions
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Death")) //compares tag to see if matches str
        {
            EndLevel();
        }
        
        if (collision.gameObject.CompareTag("PickUp")) //compares tag to see if matches str
        {
            collision.gameObject.SetActive(false);
            counter += 1;
            pickUpSound.Play();
        }

        if (collision.gameObject.CompareTag("5PickUp")) //compares tag to see if matches str
        {
            collision.gameObject.SetActive(false);
            counter += 5;
            pickUpSound.Play();
        }
        if (collision.gameObject.CompareTag("ExtraLive")) //compares tag to see if matches str
        {
            collision.gameObject.SetActive(false);
            lives += 1;
            pickUpSound.Play();
        }

        if (collision.gameObject.CompareTag("Kill")) //compares tag to see if matches str
        {
            collision.gameObject.SetActive(false);
            counter += 3;
            print("its a kill");
        }

        if (collision.gameObject.CompareTag("Enemy")) //compares tag to see if matches str
        {
            //collision.gameObject.SetActive(false);
            lives -= 1;
            
        }

        

    }
    //set the text for the coin cointer
    void SetCountText()
    {

        counter_text.text = "Coins: " + counter;
        if (counter > winAmount)
        {
            winTextObject.SetActive(true);//displays win text
        }
    }
    //set the text for lives counter
    void SetLivesText()
    {

        lives_text.text = "Lives: " + lives;
        if (lives <= 0)
        {
            death = true;
        }
    }
    //thought about doing a fade in image for the winning/ending
    void EndLevelWin(CanvasGroup imageCanvasGroup)
    {

        // timer for fade in of image over scene
        m_Timer += Time.deltaTime;
        imageCanvasGroup.alpha = m_Timer / fadeDuration;
    }
}
