﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class GameEnding : MonoBehaviour
{
    //sets all necessary variables for the sciprt
    public GameObject player;
    public TextMeshProUGUI timer_text;
    public float timeLeft;
    public GameObject winTextObject;
    public AudioSource callAudio;
    public float m_timer = 5;

    bool playerAtExit;
    bool m_IsPLayerHurt;


    
    // Start is called before the first frame update
    void Start()
    {
        winTextObject.SetActive(false);
        
    }

    // Update is called once per frame
    void Update()
    {
        
        
        
    }
    //trigger function to restart level
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject == player)
            playerAtExit = true;
        EndLevel();
        Invoke("Restart", m_timer);
    }
    //function for player winning
    void EndLevel()
    {
        if (playerAtExit == true)
            winTextObject.SetActive(true);
            
    }
    //function for the restart
    void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
